[TOC]

## adl_cbd_status_count

Purpose: 

Status: 

|    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
|---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
|  0 | date                   | date        |              |                  |           |
|  1 | hour                   | bigint      |              |                  |           |
|  2 | minute                 | bigint      |              |                  |           |
|  3 | in_trip                | bigint      |              |                  |           |
|  4 | in_station_parking     | bigint      |              |                  |           |
|  5 | in_station_out_station | bigint      |              |                  |           |
|  6 | in_station_warehouse   | bigint      |              |                  |           |
|  7 | in_station_pickup      | bigint      |              |                  |           |
|  8 | in_station_staging     | bigint      |              |                  |           |
|  9 | in_stock               | bigint      |              |                  |           |
| 10 | missing                | bigint      |              |                  |           |
| 11 | rebalancing            | bigint      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## adl_wa_status_count

Purpose: 

Status: 

|    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
|---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
|  0 | date                   | date        |              |                  |           |
|  1 | hour                   | bigint      |              |                  |           |
|  2 | minute                 | bigint      |              |                  |           |
|  3 | in_trip                | bigint      |              |                  |           |
|  4 | in_station_parking     | bigint      |              |                  |           |
|  5 | in_station_out_station | bigint      |              |                  |           |
|  6 | in_station_warehouse   | bigint      |              |                  |           |
|  7 | in_station_pickup      | bigint      |              |                  |           |
|  8 | in_station_staging     | bigint      |              |                  |           |
|  9 | in_stock               | bigint      |              |                  |           |
| 10 | missing                | bigint      |              |                  |           |
| 11 | rebalancing            | bigint      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## battery_history

Purpose: 

Status: 

|    | column_name        | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:-------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id                 | bigint                      |              |                  |           |
|  1 | scooter_id         | bigint                      |              |                  |           |
|  2 | created_at         | timestamp without time zone |              |                  |           |
|  3 | updated_at         | timestamp without time zone |              |                  |           |
|  4 | battery_number     | character varying (256)     |              |                  |           |
|  5 | remaining_capacity | smallint                    |              |                  |           |
|  6 | full_capacity      | smallint                    |              |                  |           |
|  7 | work_cycle         | smallint                    |              |                  |           |
|  8 | electricity        | double precision            |              |                  |           |
|  9 | city               | character varying (256)     |              |                  |           |
| 10 | status             | character varying (30)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## bne_status_count

Purpose: 

Status: 

|    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
|---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
|  0 | date                   | date        |              |                  |           |
|  1 | hour                   | bigint      |              |                  |           |
|  2 | minute                 | bigint      |              |                  |           |
|  3 | in_trip                | bigint      |              |                  |           |
|  4 | in_station_parking     | bigint      |              |                  |           |
|  5 | in_station_out_station | bigint      |              |                  |           |
|  6 | in_station_warehouse   | bigint      |              |                  |           |
|  7 | in_station_pickup      | bigint      |              |                  |           |
|  8 | in_station_staging     | bigint      |              |                  |           |
|  9 | in_stock               | bigint      |              |                  |           |
| 10 | missing                | bigint      |              |                  |           |
| 11 | rebalancing            | bigint      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## city_status_count

Purpose: 

Status: 

|    | column_name                   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:------------------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | city                          | character varying (32)      |              |                  |           |
|  1 | created_at                    | timestamp without time zone |              |                  |           |
|  2 | in_station_parking            | bigint                      |              |                  |           |
|  3 | in_station_out_station        | bigint                      |              |                  |           |
|  4 | in_station_pickup             | bigint                      |              |                  |           |
|  5 | in_station_staging            | bigint                      |              |                  |           |
|  6 | in_reservation                | bigint                      |              |                  |           |
|  7 | no_riding                     | bigint                      |              |                  |           |
|  8 | outside_service               | bigint                      |              |                  |           |
|  9 | admin_lock                    | bigint                      |              |                  |           |
| 10 | in_trip                       | bigint                      |              |                  |           |
| 11 | in_stock                      | bigint                      |              |                  |           |
| 12 | in_repair                     | bigint                      |              |                  |           |
| 13 | in_warehouse                  | bigint                      |              |                  |           |
| 14 | missing                       | bigint                      |              |                  |           |
| 15 | beyond_repair                 | bigint                      |              |                  |           |
| 16 | rebalancing                   | bigint                      |              |                  |           |
| 17 | rebalancing_and_available     | bigint                      |              |                  |           |
| 18 | in_trip_and_available         | bigint                      |              |                  |           |
| 19 | no_riding_and_available       | bigint                      |              |                  |           |
| 20 | outside_service_and_available | bigint                      |              |                  |           |
| 21 | admin_lock_and_available      | bigint                      |              |                  |           |
| 22 | in_reservation_and_available  | bigint                      |              |                  |           |
| 23 | in_station_and_available      | bigint                      |              |                  |           |
| 24 | in_station_ops_only           | bigint                      |              |                  |           |
| 25 | vehicle_type                  | character varying (30)      |              |                  |           |
| 26 | zone                          | character varying (32)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## command_log_history

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id            | bigint                      |              |                  |           |
|  1 | command       | character varying (1024)    |              |                  |           |
|  2 | succeed       | smallint                    |              |                  |           |
|  3 | device_id     | bigint                      |              |                  |           |
|  4 | created_at    | timestamp without time zone |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## command_response_history

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id            | bigint                      |              |                  |           |
|  1 | message       | character varying (255)     |              |                  |           |
|  2 | device_id     | bigint                      |              |                  |           |
|  3 | created_at    | timestamp without time zone |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## drw_status_count

Purpose: 

Status: 

|    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
|---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
|  0 | date                   | date        |              |                  |           |
|  1 | hour                   | bigint      |              |                  |           |
|  2 | minute                 | bigint      |              |                  |           |
|  3 | in_trip                | bigint      |              |                  |           |
|  4 | in_station_parking     | bigint      |              |                  |           |
|  5 | in_station_out_station | bigint      |              |                  |           |
|  6 | in_station_warehouse   | bigint      |              |                  |           |
|  7 | in_station_pickup      | bigint      |              |                  |           |
|  8 | in_station_staging     | bigint      |              |                  |           |
|  9 | in_stock               | bigint      |              |                  |           |
| 10 | missing                | bigint      |              |                  |           |
| 11 | rebalancing            | bigint      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## feedback_phrases

Purpose: 

Status: 

|    | column_name   | data_type               | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:------------------------|:-------------|:-----------------|:----------|
|  0 | phrases       | character varying (256) |              |                  |           |
|  1 | count         | bigint                  |              |                  |           |
|  2 | rating        | character varying (256) |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## geofence_wkb

Purpose: 

Status: 

|    | column_name    | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:---------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | fence_id       | bigint                      |              |                  |           |
|  1 | name           | character varying (150)     |              |                  |           |
|  2 | geometry       | geometry                    |              |                  |           |
|  3 | city           | character varying (20)      |              |                  |           |
|  4 | max_speed      | real                        |              |                  |           |
|  5 | status         | character varying (20)      |              |                  |           |
|  6 | hidden         | smallint                    |              |                  |           |
|  7 | start_time     | character varying (10)      |              |                  |           |
|  8 | end_time       | character varying (10)      |              |                  |           |
|  9 | weekdays       | character varying (32)      |              |                  |           |
| 10 | timed_geofence | character varying (128)     |              |                  |           |
| 11 | updated_at     | timestamp without time zone |              |                  |           |
| 12 | created_at     | timestamp without time zone |              |                  |           |
| 13 | type           | character varying (64)      |              |                  |           |
| 14 | vehicle_type   | character varying (15)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## gps_history

Purpose: 

Status: 

|    | column_name          | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:---------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | imei                 | character varying (50)      |              |                  |           |
|  1 | updated_at           | timestamp without time zone |              |                  |           |
|  2 | helmet_attached      | smallint                    |              |                  |           |
|  3 | dashboard_version    | character varying (256)     |              |                  |           |
|  4 | motor_version        | character varying (256)     |              |                  |           |
|  5 | bluetooth_version    | character varying (256)     |              |                  |           |
|  6 | bt_mac               | character varying (32)      |              |                  |           |
|  7 | is_connected         | smallint                    |              |                  |           |
|  8 | topple               | smallint                    |              |                  |           |
|  9 | remaining_range      | numeric                     |              |                  |           |
| 10 | blt_password         | character varying (128)     |              |                  |           |
| 11 | remaining_battery    | double precision            |              |                  |           |
| 12 | latitude             | double precision            |              |                  |           |
| 13 | longitude            | double precision            |              |                  |           |
| 14 | engine_off           | smallint                    |              |                  |           |
| 15 | gps_external_power   | smallint                    |              |                  |           |
| 16 | id                   | bigint                      |              |                  |           |
| 17 | created_at           | timestamp without time zone |              |                  |           |
| 18 | alarm_on             | smallint                    |              |                  |           |
| 19 | gps_battery_level    | smallint                    |              |                  |           |
| 20 | status               | character varying (30)      |              |                  |           |
| 21 | location_source      | character varying (50)      |              |                  |           |
| 22 | position_active_time | timestamp without time zone |              |                  |           |
| 23 | voltage              | double precision            |              |                  |           |
| 24 | iot_version          | character varying (50)      |              |                  |           |
| 25 | dbu_hw               | character varying (10)      |              |                  |           |
| 26 | protocol             | character varying (50)      |              |                  |           |
| 27 | local_geofence       | smallint                    |              |                  |           |
| 28 | version_list         | super                       |              |                  |           |
| 29 | white_noise          | smallint                    |              |                  |           |
| 30 | mode                 | smallint                    |              |                  |           |
| 31 | local_geo_status     | character varying (256)     |              |                  |           |
| 32 | hdop                 | double precision            |              |                  |           |
| 33 | vin                  | double precision            |              |                  |           |
| 34 | ride_mile            | bigint                      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## gross_revenue_history

Purpose: 

Status: 

|    | column_name             | data_type                | Definition   | Related key(s)   | Remarks   |
|---:|:------------------------|:-------------------------|:-------------|:-----------------|:----------|
|  0 | order_id                | bigint                   |              |                  |           |
|  1 | order_updated_utc       | timestamp with time zone |              |                  |           |
|  2 | pass_amount             | double precision         |              |                  |           |
|  3 | gross_rev               | double precision         |              |                  |           |
|  4 | cap_off_fee             | double precision         |              |                  |           |
|  5 | waived_trip_fare        | double precision         |              |                  |           |
|  6 | engine_off_fee          | double precision         |              |                  |           |
|  7 | transfer_ride_unlocking | double precision         |              |                  |           |
|  8 | pass_unlock_fee         | double precision         |              |                  |           |
|  9 | coupon                  | double precision         |              |                  |           |
| 10 | adjust_off              | double precision         |              |                  |           |
| 11 | vip_fee                 | double precision         |              |                  |           |
| 12 | short_trip_fee          | double precision         |              |                  |           |
| 13 | free_morning_fee        | double precision         |              |                  |           |
| 14 | payable_amount          | double precision         |              |                  |           |
| 15 | created_at              | timestamp with time zone |              |                  |           |
| 16 | city                    | character varying (10)   |              |                  |           |
| 17 | city_base_fee           | double precision         |              |                  |           |
| 18 | city_unit_fee           | double precision         |              |                  |           |
| 19 | base_fee                | double precision         |              |                  |           |
| 20 | unit_fee                | double precision         |              |                  |           |
| 21 | is_vip                  | smallint                 |              |                  |           |
| 22 | is_short_trip           | smallint                 |              |                  |           |
| 23 | transfer_ride           | smallint                 |              |                  |           |
| 24 | pass_id                 | bigint                   |              |                  |           |
| 25 | total_minutes           | bigint                   |              |                  |           |
| 26 | engine_off_minutes      | bigint                   |              |                  |           |
| 27 | vehicle_type            | character varying (15)   |              |                  |           |
| 28 | order_status            | character varying (15)   |              |                  |           |
| 29 | order_created_utc       | timestamp with time zone |              |                  |           |
| 30 | currency                | character varying (10)   |              |                  |           |
| 31 | deposit_captured        | double precision         |              |                  |           |
| 32 | load_time               | timestamp with time zone |              |                  |           |
| 33 | unit_fee_discount       | double precision         |              |                  |           |
| 34 | base_fee_discount       | double precision         |              |                  |           |
| 35 | partial_pass_unlock_fee | double precision         |              |                  |           |
| 36 | helmet_replacement_fee  | double precision         |              |                  |           |
| 37 | convenience_fee         | double precision         |              |                  |           |
| 38 | gross_trip_fare         | double precision         |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## incident_report

Purpose: 

Status: 

|    | column_name             | data_type                 | Definition   | Related key(s)   | Remarks   |
|---:|:------------------------|:--------------------------|:-------------|:-----------------|:----------|
|  0 | incident_id             | bigint                    |              |                  |           |
|  1 | city                    | character varying (8)     |              |                  |           |
|  2 | business_risk_level     | character varying (16)    |              |                  |           |
|  3 | risk_level              | character varying (16)    |              |                  |           |
|  4 | status                  | character varying (32)    |              |                  |           |
|  5 | type                    | character varying (32)    |              |                  |           |
|  6 | incident_time           | timestamp with time zone  |              |                  |           |
|  7 | aware_time              | timestamp with time zone  |              |                  |           |
|  8 | police                  | boolean                   |              |                  |           |
|  9 | ticket_url              | character varying (256)   |              |                  |           |
| 10 | short_description       | character varying (65535) |              |                  |           |
| 11 | incident_description    | character varying (65535) |              |                  |           |
| 12 | remarks                 | character varying (65535) |              |                  |           |
| 13 | user_involved           | character varying (32)    |              |                  |           |
| 14 | third_party_involved    | character varying (32)    |              |                  |           |
| 15 | location                | character varying (65535) |              |                  |           |
| 16 | created_user            | character varying (16)    |              |                  |           |
| 17 | created_user_role       | character varying (32)    |              |                  |           |
| 18 | updated_user            | character varying (16)    |              |                  |           |
| 19 | updated_user_role       | character varying (32)    |              |                  |           |
| 20 | platform                | character varying (16)    |              |                  |           |
| 21 | version                 | double precision          |              |                  |           |
| 22 | created_at              | timestamp with time zone  |              |                  |           |
| 23 | updated_at              | timestamp with time zone  |              |                  |           |
| 24 | updated_by              | bigint                    |              |                  |           |
| 25 | created_by              | bigint                    |              |                  |           |
| 26 | latitude                | double precision          |              |                  |           |
| 27 | longitude               | double precision          |              |                  |           |
| 28 | category                | character varying (16)    |              |                  |           |
| 29 | reason                  | character varying (65535) |              |                  |           |
| 30 | source                  | character varying (32)    |              |                  |           |
| 31 | edm_sent                | boolean                   |              |                  |           |
| 32 | user_id                 | bigint                    |              |                  |           |
| 33 | injury                  | character varying (16)    |              |                  |           |
| 34 | injury_severity         | character varying (16)    |              |                  |           |
| 35 | medical                 | character varying (16)    |              |                  |           |
| 36 | email                   | character varying (64)    |              |                  |           |
| 37 | paramedic               | character varying (16)    |              |                  |           |
| 38 | user_description        | character varying (65535) |              |                  |           |
| 39 | trip_id                 | bigint                    |              |                  |           |
| 40 | start_time              | timestamp with time zone  |              |                  |           |
| 41 | end_time                | timestamp with time zone  |              |                  |           |
| 42 | trip_status             | character varying (16)    |              |                  |           |
| 43 | start_station_id        | integer                   |              |                  |           |
| 44 | start_station_name      | character varying (32)    |              |                  |           |
| 45 | end_station_id          | integer                   |              |                  |           |
| 46 | end_station_name        | character varying (32)    |              |                  |           |
| 47 | start_zone_code         | character varying (16)    |              |                  |           |
| 48 | start_zone_name         | character varying (32)    |              |                  |           |
| 49 | end_zone_code           | character varying (16)    |              |                  |           |
| 50 | end_zone_name           | character varying (32)    |              |                  |           |
| 51 | deck_id                 | character varying (32)    |              |                  |           |
| 52 | generation              | character varying (8)     |              |                  |           |
| 53 | device_id               | bigint                    |              |                  |           |
| 54 | imei                    | character varying (32)    |              |                  |           |
| 55 | qr_code                 | character varying (16)    |              |                  |           |
| 56 | sticker_id              | character varying (16)    |              |                  |           |
| 57 | party                   | character varying (16)    |              |                  |           |
| 58 | third_party_category    | character varying (16)    |              |                  |           |
| 59 | third_party_description | character varying (65535) |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## n3_status_history

Purpose: 

Status: 

|    | column_name             | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:------------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id                      | bigint                      |              |                  |           |
|  1 | electric_hall           | smallint                    |              |                  |           |
|  2 | a_phase_current         | smallint                    |              |                  |           |
|  3 | b_phase_current         | smallint                    |              |                  |           |
|  4 | c_phase_current         | smallint                    |              |                  |           |
|  5 | dc_current              | smallint                    |              |                  |           |
|  6 | accelerator             | smallint                    |              |                  |           |
|  7 | dashboard_communication | smallint                    |              |                  |           |
|  8 | bms_communication       | smallint                    |              |                  |           |
|  9 | brake                   | smallint                    |              |                  |           |
| 10 | iot_communication       | smallint                    |              |                  |           |
| 11 | ble_communication       | smallint                    |              |                  |           |
| 12 | lowe_battery            | smallint                    |              |                  |           |
| 13 | low_battery_protection  | smallint                    |              |                  |           |
| 14 | topple                  | smallint                    |              |                  |           |
| 15 | lock_status             | character varying (32)      |              |                  |           |
| 16 | battery_percentage      | double precision            |              |                  |           |
| 17 | device_id               | bigint                      |              |                  |           |
| 18 | created_at              | timestamp without time zone |              |                  |           |
| 19 | single_range            | double precision            |              |                  |           |
| 20 | speed                   | double precision            |              |                  |           |
| 21 | voltage                 | double precision            |              |                  |           |
| 22 | electricity             | double precision            |              |                  |           |
| 23 | helmet_attached         | smallint                    |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## scooter_history

Purpose: 

Status: 

|    | column_name        | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:-------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id                 | bigint                      |              |                  |           |
|  1 | imei               | character varying (50)      |              |                  |           |
|  2 | station_id         | double precision            |              |                  |           |
|  3 | qr_code            | character varying (50)      |              |                  |           |
|  4 | deck_id            | character varying (50)      |              |                  |           |
|  5 | mac                | character varying (70)      |              |                  |           |
|  6 | status             | character varying (30)      |              |                  |           |
|  7 | city               | character varying (20)      |              |                  |           |
|  8 | created_at         | timestamp without time zone |              |                  |           |
|  9 | updated_at         | timestamp without time zone |              |                  |           |
| 10 | deleted            | smallint                    |              |                  |           |
| 11 | last_sanitize_time | timestamp without time zone |              |                  |           |
| 12 | zone               | character varying (20)      |              |                  |           |
| 13 | type               | character varying (32)      |              |                  |           |
| 14 | generation         | character varying (16)      |              |                  |           |
| 15 | sticker            | character varying (15)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_battery_history

Purpose: 

Status: 

|    | column_name        | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:-------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | op                 | character varying (10)      |              |                  |           |
|  1 | id                 | bigint                      |              |                  |           |
|  2 | scooter_id         | bigint                      |              |                  |           |
|  3 | created_at         | timestamp without time zone |              |                  |           |
|  4 | updated_at         | timestamp without time zone |              |                  |           |
|  5 | battery_number     | character varying (256)     |              |                  |           |
|  6 | remaining_capacity | smallint                    |              |                  |           |
|  7 | full_capacity      | smallint                    |              |                  |           |
|  8 | work_cycle         | smallint                    |              |                  |           |
|  9 | electricity        | double precision            |              |                  |           |
| 10 | city               | character varying (256)     |              |                  |           |
| 11 | status             | character varying (30)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_command_log_history

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id            | bigint                      |              |                  |           |
|  1 | command       | character varying (1024)    |              |                  |           |
|  2 | succeed       | smallint                    |              |                  |           |
|  3 | device_id     | bigint                      |              |                  |           |
|  4 | created_at    | timestamp without time zone |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_command_response_history

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id            | bigint                      |              |                  |           |
|  1 | message       | character varying (255)     |              |                  |           |
|  2 | device_id     | bigint                      |              |                  |           |
|  3 | created_at    | timestamp without time zone |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_gps_history

Purpose: 

Status: 

|    | column_name          | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:---------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | op                   | character varying (10)      |              |                  |           |
|  1 | id                   | bigint                      |              |                  |           |
|  2 | imei                 | character varying (50)      |              |                  |           |
|  3 | created_at           | timestamp without time zone |              |                  |           |
|  4 | updated_at           | timestamp without time zone |              |                  |           |
|  5 | remaining_battery    | double precision            |              |                  |           |
|  6 | latitude             | double precision            |              |                  |           |
|  7 | longitude            | double precision            |              |                  |           |
|  8 | engine_off           | smallint                    |              |                  |           |
|  9 | gps_battery_level    | smallint                    |              |                  |           |
| 10 | gps_external_power   | smallint                    |              |                  |           |
| 11 | status               | character varying (30)      |              |                  |           |
| 12 | alarm_on             | smallint                    |              |                  |           |
| 13 | version              | character varying (256)     |              |                  |           |
| 14 | voltage              | double precision            |              |                  |           |
| 15 | apn                  | character varying (256)     |              |                  |           |
| 16 | iccid                | character varying (256)     |              |                  |           |
| 17 | location_source      | character varying (50)      |              |                  |           |
| 18 | position_active_time | timestamp without time zone |              |                  |           |
| 19 | protocol             | character varying (50)      |              |                  |           |
| 20 | normal_speed         | smallint                    |              |                  |           |
| 21 | sport_speed          | smallint                    |              |                  |           |
| 22 | dashboard_version    | character varying (256)     |              |                  |           |
| 23 | motor_version        | character varying (256)     |              |                  |           |
| 24 | bluetooth_version    | character varying (256)     |              |                  |           |
| 25 | helmet_attached      | smallint                    |              |                  |           |
| 26 | no_parking_light     | smallint                    |              |                  |           |
| 27 | wrench_light         | smallint                    |              |                  |           |
| 28 | bt_mac               | character varying (32)      |              |                  |           |
| 29 | is_connected         | smallint                    |              |                  |           |
| 30 | topple               | smallint                    |              |                  |           |
| 31 | remaining_range      | numeric                     |              |                  |           |
| 32 | blt_password         | character varying (128)     |              |                  |           |
| 33 | iot_version          | character varying (50)      |              |                  |           |
| 34 | dbu_hw               | character varying (10)      |              |                  |           |
| 35 | local_geofence       | smallint                    |              |                  |           |
| 36 | version_list         | super                       |              |                  |           |
| 37 | white_noise          | smallint                    |              |                  |           |
| 38 | mode                 | smallint                    |              |                  |           |
| 39 | local_geo_status     | character varying (256)     |              |                  |           |
| 40 | hdop                 | double precision            |              |                  |           |
| 41 | vin                  | double precision            |              |                  |           |
| 42 | ride_mile            | bigint                      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_n3_status_history

Purpose: 

Status: 

|    | column_name             | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:------------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | id                      | bigint                      |              |                  |           |
|  1 | electric_hall           | smallint                    |              |                  |           |
|  2 | a_phase_current         | smallint                    |              |                  |           |
|  3 | b_phase_current         | smallint                    |              |                  |           |
|  4 | c_phase_current         | smallint                    |              |                  |           |
|  5 | dc_current              | smallint                    |              |                  |           |
|  6 | accelerator             | smallint                    |              |                  |           |
|  7 | dashboard_communication | smallint                    |              |                  |           |
|  8 | bms_communication       | smallint                    |              |                  |           |
|  9 | brake                   | smallint                    |              |                  |           |
| 10 | iot_communication       | smallint                    |              |                  |           |
| 11 | ble_communication       | smallint                    |              |                  |           |
| 12 | lowe_battery            | smallint                    |              |                  |           |
| 13 | low_battery_protection  | smallint                    |              |                  |           |
| 14 | topple                  | smallint                    |              |                  |           |
| 15 | lock_status             | character varying (32)      |              |                  |           |
| 16 | battery_percentage      | double precision            |              |                  |           |
| 17 | device_id               | bigint                      |              |                  |           |
| 18 | created_at              | timestamp without time zone |              |                  |           |
| 19 | single_range            | double precision            |              |                  |           |
| 20 | speed                   | double precision            |              |                  |           |
| 21 | voltage                 | double precision            |              |                  |           |
| 22 | electricity             | double precision            |              |                  |           |
| 23 | helmet_attached         | smallint                    |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## staging_scooter_history

Purpose: 

Status: 

|    | column_name        | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:-------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | op                 | character varying (10)      |              |                  |           |
|  1 | id                 | bigint                      |              |                  |           |
|  2 | imei               | character varying (50)      |              |                  |           |
|  3 | station_id         | double precision            |              |                  |           |
|  4 | qr_code            | character varying (50)      |              |                  |           |
|  5 | deck_id            | character varying (50)      |              |                  |           |
|  6 | mac                | character varying (70)      |              |                  |           |
|  7 | ic_card            | double precision            |              |                  |           |
|  8 | status             | character varying (30)      |              |                  |           |
|  9 | city               | character varying (20)      |              |                  |           |
| 10 | created_at         | timestamp without time zone |              |                  |           |
| 11 | updated_at         | timestamp without time zone |              |                  |           |
| 12 | version            | integer                     |              |                  |           |
| 13 | deleted            | smallint                    |              |                  |           |
| 14 | image_id           | character varying (256)     |              |                  |           |
| 15 | generation         | character varying (16)      |              |                  |           |
| 16 | zone               | character varying (256)     |              |                  |           |
| 17 | licence            | character varying (256)     |              |                  |           |
| 18 | type               | character varying (256)     |              |                  |           |
| 19 | last_test_time     | timestamp without time zone |              |                  |           |
| 20 | helmet_lock        | smallint                    |              |                  |           |
| 21 | last_sanitize_time | timestamp without time zone |              |                  |           |
| 22 | sticker            | character varying (15)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## status_count

Purpose: 

Status: 

|    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
|---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
|  0 | date                   | date        |              |                  |           |
|  1 | hour                   | bigint      |              |                  |           |
|  2 | minute                 | bigint      |              |                  |           |
|  3 | in_trip                | bigint      |              |                  |           |
|  4 | in_station_parking     | bigint      |              |                  |           |
|  5 | in_station_out_station | bigint      |              |                  |           |
|  6 | in_station_warehouse   | bigint      |              |                  |           |
|  7 | in_station_pickup      | bigint      |              |                  |           |
|  8 | in_station_staging     | bigint      |              |                  |           |
|  9 | in_stock               | bigint      |              |                  |           |
| 10 | missing                | bigint      |              |                  |           |
| 11 | rebalancing            | bigint      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_detail

Purpose: 

Status: 

|    | column_name         | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | start_time          | timestamp without time zone |              |                  |           |
|  1 | trip_id             | bigint                      |              |                  |           |
|  2 | end_time            | timestamp without time zone |              |                  |           |
|  3 | user_id             | bigint                      |              |                  |           |
|  4 | city                | character varying (20)      |              |                  |           |
|  5 | device_id           | bigint                      |              |                  |           |
|  6 | trip_status         | character varying (16)      |              |                  |           |
|  7 | total_minutes       | integer                     |              |                  |           |
|  8 | total_mileage       | double precision            |              |                  |           |
|  9 | start_station_id    | bigint                      |              |                  |           |
| 10 | returned_station_id | bigint                      |              |                  |           |
| 11 | force_end           | smallint                    |              |                  |           |
| 12 | bt_unlock           | smallint                    |              |                  |           |
| 13 | auto_end            | smallint                    |              |                  |           |
| 14 | temp_lock           | smallint                    |              |                  |           |
| 15 | is_short_trip       | smallint                    |              |                  |           |
| 16 | is_vip              | smallint                    |              |                  |           |
| 17 | zone                | character varying (20)      |              |                  |           |
| 18 | group_id            | character varying (64)      |              |                  |           |
| 19 | engine_off_minutes  | bigint                      |              |                  |           |
| 20 | user_name           | character varying (100)     |              |                  |           |
| 21 | email               | character varying (100)     |              |                  |           |
| 22 | gps_id              | bigint                      |              |                  |           |
| 23 | transfer_ride       | smallint                    |              |                  |           |
| 24 | helmet_returned     | smallint                    |              |                  |           |
| 25 | app_version         | character varying (20)      |              |                  |           |
| 26 | start_latitude      | double precision            |              |                  |           |
| 27 | start_longitude     | double precision            |              |                  |           |
| 28 | end_latitude        | double precision            |              |                  |           |
| 29 | end_longitude       | double precision            |              |                  |           |
| 30 | start_no_riding     | smallint                    |              |                  |           |
| 31 | end_no_riding       | smallint                    |              |                  |           |
| 32 | imei                | character varying (32)      |              |                  |           |
| 33 | battery_no          | character varying (64)      |              |                  |           |
| 34 | vehicle_type        | character varying (32)      |              |                  |           |
| 35 | start_zone          | character varying (20)      |              |                  |           |
| 36 | end_zone            | character varying (20)      |              |                  |           |
| 37 | campaign_id         | bigint                      |              |                  |           |
| 38 | order_amount        | real                        |              |                  |           |
| 39 | coupon_amount       | real                        |              |                  |           |
| 40 | trip_type           | character varying (100)     |              |                  |           |
| 41 | protocol            | character varying (20)      |              |                  |           |
| 42 | helmet_selfie       | smallint                    |              |                  |           |
| 43 | selfie_checked      | smallint                    |              |                  |           |
| 44 | parking_photo       | smallint                    |              |                  |           |
| 45 | rating              | integer                     |              |                  |           |
| 46 | is_avas             | smallint                    |              |                  |           |
| 47 | is_rtk              | smallint                    |              |                  |           |
| 48 | local_geofence      | smallint                    |              |                  |           |
| 49 | topple              | smallint                    |              |                  |           |
| 50 | swerving            | smallint                    |              |                  |           |
| 51 | helmet_unlock       | smallint                    |              |                  |           |
| 52 | helmet_available    | smallint                    |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_start_end_locations

Purpose: 

Status: 

|    | column_name   | data_type               | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:------------------------|:-------------|:-----------------|:----------|
|  0 | device_id     | integer                 |              |                  |           |
|  1 | el            | character varying (256) |              |                  |           |
|  2 | end_lat       | double precision        |              |                  |           |
|  3 | end_lon       | double precision        |              |                  |           |
|  4 | end_time      | character varying (256) |              |                  |           |
|  5 | sl            | character varying (256) |              |                  |           |
|  6 | start_lat     | double precision        |              |                  |           |
|  7 | start_lon     | double precision        |              |                  |           |
|  8 | start_time    | character varying (256) |              |                  |           |
|  9 | city          | character varying (256) |              |                  |           |
| 10 | trip_id       | integer                 |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_start_end_wkb

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | start_time    | timestamp without time zone |              |                  |           |
|  1 | trip_id       | integer                     |              |                  |           |
|  2 | scooter_id    | integer                     |              |                  |           |
|  3 | gps_id        | integer                     |              |                  |           |
|  4 | start_lat     | double precision            |              |                  |           |
|  5 | start_lon     | double precision            |              |                  |           |
|  6 | start_point   | geometry                    |              |                  |           |
|  7 | end_time      | timestamp without time zone |              |                  |           |
|  8 | end_lat       | double precision            |              |                  |           |
|  9 | end_lon       | double precision            |              |                  |           |
| 10 | end_point     | geometry                    |              |                  |           |
| 11 | city          | character varying (256)     |              |                  |           |
| 12 | vehicle_type  | character varying (32)      |              |                  |           |
| 13 | imei          | character varying (32)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_status

Purpose: 

Status: 

|    | column_name        | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:-------------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | created_at         | timestamp without time zone |              |                  |           |
|  1 | trip_id            | bigint                      |              |                  |           |
|  2 | city               | character varying (20)      |              |                  |           |
|  3 | voltage            | double precision            |              |                  |           |
|  4 | speed              | double precision            |              |                  |           |
|  5 | electricity        | double precision            |              |                  |           |
|  6 | battery_percentage | double precision            |              |                  |           |
|  7 | imei               | character varying (32)      |              |                  |           |
|  8 | protocol           | character varying (10)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_waypoints

Purpose: 

Status: 

|    | column_name   | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:--------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | created_at    | timestamp without time zone |              |                  |           |
|  1 | trip_id       | bigint                      |              |                  |           |
|  2 | city          | character varying (20)      |              |                  |           |
|  3 | device_id     | bigint                      |              |                  |           |
|  4 | latitude      | double precision            |              |                  |           |
|  5 | longitude     | double precision            |              |                  |           |
|  6 | imei          | character varying (32)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)

## trip_waypoints_detailed

Purpose: 

Status: 

|    | column_name    | data_type                   | Definition   | Related key(s)   | Remarks   |
|---:|:---------------|:----------------------------|:-------------|:-----------------|:----------|
|  0 | created_at     | timestamp without time zone |              |                  |           |
|  1 | trip_id        | bigint                      |              |                  |           |
|  2 | city           | character varying (20)      |              |                  |           |
|  3 | device_id      | bigint                      |              |                  |           |
|  4 | latitude       | double precision            |              |                  |           |
|  5 | longitude      | double precision            |              |                  |           |
|  6 | position_speed | double precision            |              |                  |           |
|  7 | imei           | character varying (32)      |              |                  |           |

[Return to top](https://bitbucket.org/calvin_huang/schema_doc/src/master/catalog_sample_for_australia.md#top)
